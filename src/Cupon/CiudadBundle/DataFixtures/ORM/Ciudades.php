<?php
/**
 * Created by PhpStorm.
 * User: dbertoni
 * Date: 25/07/14
 * Time: 13:16
 */

namespace Cupon\CiudadBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cupon\CiudadBundle\Entity\Ciudad;
use Cupon\OfertaBundle\Util\Util;

class Ciudades implements FixtureInterface
{
    /**
     * Fixture Execution order
     *
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param $manager
     */
    function load(ObjectManager $manager)
    {
        $ciudades = [
            array('nombre'=>'General Alvear'),
            array('nombre'=>'Tapalque'),
            array('nombre'=>'Saladillo'),
            array('nombre'=>'Olavarria'),
            array('nombre'=>'25 de Mayo'),
            array('nombre'=>'Carlos Casares'),
            array('nombre'=>'Tandil'),
            array('nombre'=>'Laprida'),
            array('nombre'=>'Roque Perez'),
            array('nombre'=>'Azul'),
            array('nombre'=>'Coronel Suarez'),
            array('nombre'=>'Mar del Plata'),
            array('nombre'=>'Necochea'),
            array('nombre'=>'Coronel Brandsen'),
        ];

        foreach($ciudades as $ciudad)
        {
            $entidad = new Ciudad();
            $entidad->setNombre($ciudad['nombre']);

            $manager->persist($entidad);
        }

        $manager->flush();
    }
}