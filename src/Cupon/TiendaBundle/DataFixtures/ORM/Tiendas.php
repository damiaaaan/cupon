<?php
/**
 * Created by PhpStorm.
 * User: damianb
 * Date: 26/07/14
 * Time: 12:54
 */

namespace Cupon\TiendaBundle\DataFixtures\ORM;


use Cupon\CiudadBundle\Entity\Ciudad;
use Cupon\TiendaBundle\Entity\Tienda;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;


class Tiendas extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * Fixture execution order
     *
     * @return int
     */
    public function getOrder()
    {
        return 20;
    }

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param $manager
     */
    function load(ObjectManager $manager)
    {
        //Obtener todas las ciudades
        $ciudades = $manager->getRepository('CiudadBundle:Ciudad')->findAll();

        $i = 1;
        foreach($ciudades as $ciudad)
        {
            $numeroDeTiendas = rand(2, 5);
            for($j = 1; $j <= $numeroDeTiendas; $j++)
            {
                $tienda = new Tienda();

                $tienda->setNombre($this->getNombre());
                $tienda->setLogin('tienda'.$i);
                $tienda->setSalt(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36));

                $passwordEnClaro = 'tienda'.$i;

                $encoder = $this->container->get('security.encoder_factory')->getEncoder($tienda);
                $passwordCodificado = $encoder->encodePassword($passwordEnClaro, $tienda->getSalt());

                $tienda->setPassword($passwordCodificado);

                $tienda->setDescripcion($this->getDescripcion());
                $tienda->setDireccion($this->getDireccion($ciudad));
                $tienda->setCiudad($ciudad);

                $manager->persist($ciudad);
                $i++;

            }
        }

        $manager->flush();
    }

    /**
     * Genera aleatoriamente Nombre de una Tienda
     *
     * @return string Nombre
     */
    private function getNombre()
    {
        $prefijos = ['Restaurante', 'Cafeteria', 'Bar', 'Pub', 'Pizza', 'Burger'];

        $nombres = [
            'Lorem ipsum', 'Sit amet', 'Consectetur', 'Adipiscing elit',
            'Nec sapien', 'Tincidunt', 'Facilisis', 'Nulla scelerisque',
            'Blandit ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enim sit'
        ];

        return $prefijos[array_rand($prefijos)].' '.$nombres[array_rand($nombres)];

    }

    /**
     * Genera descripcion aleatoria de Descripcion
     *
     * @return string Descripcion
     */
    private function getDescripcion()
    {
        $descripcion = '';

        $frases = array_flip(['Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'Mauris ultricies nunc nec sapien tincidunt facilisis.',
            'Nulla scelerisque blandit ligula eget hendrerit.',
            'Sed malesuada, enim sit amet ultricies semper, elit leo lacinia massa, in tempus nisl ipsum quis libero.',
            'Aliquam molestie neque non augue molestie bibendum.',
            'Pellentesque ultricies erat ac lorem pharetra vulputate.',
            'Donec dapibus blandit odio, in auctor turpis commodo ut.',
            'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
            'Nam rhoncus lorem sed libero hendrerit accumsan.',
            'Maecenas non erat eu justo rutrum condimentum.',
            'Suspendisse leo tortor, tempus in lacinia sit amet, varius eu urna.',
            'Phasellus eu leo tellus, et accumsan libero.',
            'Pellentesque fringilla ipsum nec justo tempus elementum.',
            'Aliquam dapibus metus aliquam ante lacinia blandit.',
            'Donec ornare lacus vitae dolor imperdiet vitae ultricies nibh congue.'
        ]);

        $numeroFrases = rand(3, 6);

        return implode(' ', array_rand($frases, $numeroFrases));

    }

    /**
     * Genera aleatoriamente un direccion
     *
     * @return string Direccion
     */
    private function getDireccion(Ciudad $ciudad)
    {
        $prefijos = ['Avenida', 'Calle', 'Plaza'];

        $nombres = ['Lorem', 'Ipsum', 'Sitamet', 'Consectetur', 'Adipiscing',
            'Necsapien', 'Tincidunt', 'Facilisis', 'Nulla', 'Scelerisque',
            'Blandit', 'Ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enimsit'
        ];

        return $prefijos[array_rand($prefijos)].' '.$nombres[array_rand($nombres)].', '.rand(1, 100)."\n".
            $this->getCodigoPostal().' '. $ciudad->getNombre();
    }

    /**
     * Genera aleatoriamente un codigo postal para una ciudad
     *
     * @return string Codigo Postal
     */
    private function getCodigoPostal()
    {
        return sprintf('%02s%03s', rand(1, 52), rand(0, 999));
    }
}