<?php
/**
 * Created by PhpStorm.
 * User: dbertoni
 * Date: 25/07/14
 * Time: 15:35
 */

namespace Cupon\OfertaBundle\Util;


class Util {

    /**
     * Get Slug
     *
     * @param String $cadena
     * @param string $separador
     * @return string
     */
    static public function getSlug($cadena, $separador = '-')
    {
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $cadena);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $separador));
        $slug = preg_replace("/[\/_|+ -]+/", $separador, $slug);

        return $slug;

    }

} 