<?php
/**
 * Created by PhpStorm.
 * User: dbertoni
 * Date: 24/07/14
 * Time: 13:28
 */
namespace Cupon\OfertaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SitioController extends Controller
{
    public function estaticaAction($pagina)
    {
        return $this->render('OfertaBundle:Sitio:'.$pagina.'.html.twig');
    }
}