<?php
/**
 * Created by PhpStorm.
 * User: dbertoni
 * Date: 25/07/14
 * Time: 09:46
 */

namespace Cupon\OfertaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Venta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Cupon\OfertaBundle\Entity\VentaRepository")
 */
class Venta {

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Id
     *
     * @ORM\ManyToOne(targetEntity="Cupon\OfertaBundle\Entity\Oferta")
     */
    protected $oferta;

    /**
     * @ORM\Id
     *
     * @ORM\ManyToOne(targetEntity="Cupon\UsuarioBundle\Entity\Usuario")
     */
    protected $usuario;

    /**
     * Set fecha
     *
     * @param $fecha
     * @return \DateTime
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set oferta
     *
     * @param \Cupon\OfertaBundle\Entity\Oferta $oferta
     * @return Venta
     */
    public function setOferta(\Cupon\OfertaBundle\Entity\Oferta $oferta)
    {
        $this->oferta = $oferta;
        return $this;
    }

    /**
     * Get Oferta
     *
     * @return Oferta
     */
    public function  getOferta()
    {
        return $this->oferta;
    }

    /**
     * Set usuario
     *
     * @param \Cupon\UsuarioBundle\Entity\Usuario $usuario
     * @return Venta
     */
    public function setUsuario(\Cupon\UsuarioBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     *
     * @return Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

} 